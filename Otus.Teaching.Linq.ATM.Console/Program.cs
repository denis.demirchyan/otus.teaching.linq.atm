﻿using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();
            
            //TODO: Далее выводим результаты разработанных LINQ запросов

            var isRun = true;
            while (isRun)
            {
                ShowMenu();
                var action = System.Console.ReadLine();
                switch (action)
                {
                    case "1":
                    {
                        var (login, password) = InputLoginPasswordUser();
                        var user = atmManager.GetUserInfo(login, password);
                        System.Console.WriteLine(user != null ? user.ToString() : "Not found.");
                        break;
                    }
                    case "2":
                    {
                        var (login, password) = InputLoginPasswordUser();
                        var userAccounts = atmManager.GetUserAccounts(login, password);
                        if (userAccounts == null || !userAccounts.Any())
                        {
                            System.Console.WriteLine("Not found.");
                        }
                        else
                        {
                            foreach (var userAccount in userAccounts)
                            {
                                System.Console.WriteLine(userAccount.ToString());
                            }
                        }
                        break;
                    }
                    case "3":
                    {
                        var (login, password) = InputLoginPasswordUser();
                        var accountsAndHistories = atmManager.GetUserAccountsAndHistory(login, password);
                        if (accountsAndHistories == null || !accountsAndHistories.Any())
                        {
                            System.Console.WriteLine("Not found.");
                        }
                        else
                        {
                            foreach (var (account, operationsHistories) in accountsAndHistories)
                            {
                                System.Console.WriteLine("Аккаунт:");
                                System.Console.WriteLine(account.ToString());
                                System.Console.WriteLine("Операции:");
                                foreach (var history in operationsHistories)
                                {
                                    System.Console.WriteLine(history.ToString());
                                }
                            }
                        }
                        break;
                    }
                    case "4":
                        var usersAccountsOperationsInput = atmManager.GetUsersAccountsOperationsInput();
                        if (usersAccountsOperationsInput == null || !usersAccountsOperationsInput.Any())
                        {
                            System.Console.WriteLine("Not found.");
                        }
                        else
                        {
                            foreach (var (user, accountsHistories) in usersAccountsOperationsInput)
                            {
                                System.Console.WriteLine("Пользователь:");
                                System.Console.WriteLine(user.ToString());
                                foreach (var (account, operationsHistories) in accountsHistories)
                                {
                                    System.Console.WriteLine("Аккаунт:");
                                    System.Console.WriteLine(account.ToString());
                                    System.Console.WriteLine("Операции:");
                                    foreach (var operation in operationsHistories)
                                    {
                                        System.Console.WriteLine(operation.ToString());
                                    }
                                }
                            }
                        }
                        break;
                    case "5":
                        System.Console.WriteLine("Введите N:");
                        var n = decimal.Parse(System.Console.ReadLine() ?? string.Empty);
                        var usersAccountSumMoreN = atmManager.GetUsersAccountSumMoreN(n);
                        if (usersAccountSumMoreN == null || !usersAccountSumMoreN.Any())
                        {
                            System.Console.WriteLine("Not found");
                        }
                        else
                        {
                            foreach (var user in usersAccountSumMoreN)
                            {
                                System.Console.WriteLine(user.ToString());
                            }
                        }
                        break;
                    case "0":
                        isRun = false;
                        break;

                }
            }
            
            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }

        private static void ShowMenu()
        {
            System.Console.WriteLine("Выберите действие:");
            System.Console.WriteLine("1. Вывод информации о заданном аккаунте по логину и паролю;");
            System.Console.WriteLine("2. Вывод данных о всех счетах заданного пользователя;");
            System.Console.WriteLine("3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту;");
            System.Console.WriteLine("4. Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта;");
            System.Console.WriteLine("5. Вывод данных о всех пользователях у которых на счёте сумма больше N;");
            System.Console.WriteLine("0. Завершение работы;");
        }

        private static (string login, string password) InputLoginPasswordUser()
        {
            System.Console.WriteLine("Введите логин:");
            var login = System.Console.ReadLine();
            System.Console.WriteLine("Введите пароль:");
            var password = System.Console.ReadLine();
            
            return (login, password);
        }
    }
}