﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }
        
        //TODO: Добавить методы получения данных для банкомата

        /// <summary>
        /// Получить пользователя по логину и паролю.
        /// </summary>
        /// <param name="login">Логин.</param>
        /// <param name="password">Пароль.</param>
        /// <returns>Пользователь.</returns>
        public User GetUserInfo(string login, string password)
        {
            var userInfo = (from user in Users
                            where user.Login.Equals(login) && user.Password.Equals(password)
                            select user)
                            .SingleOrDefault();
            return userInfo;
        }

        /// <summary>
        /// Получить все аккаунты пользователя по логину и паролю.
        /// </summary>
        /// <param name="login">Логин.</param>
        /// <param name="password">Пароль.</param>
        /// <returns>Аккаунты пользователя.</returns>
        public IEnumerable<Account> GetUserAccounts(string login, string password)
        {
            var userAccounts = from user in Users
                join account in Accounts on user.Id equals account.UserId
                where user.Login.Equals(login) && user.Password.Equals(password)
                select account;
            return userAccounts;
        }

        /// <summary>
        /// Получить все аккаунты и историю операций пользователя по логину и паролю.
        /// </summary>
        /// <param name="login">Логин.</param>
        /// <param name="password">Пароль.</param>
        /// <returns>Аккаунты и операции.</returns>
        public Dictionary<Account, IEnumerable<OperationsHistory>> GetUserAccountsAndHistory(string login, string password)
        {
            var accountsAndHistories = (from user in Users
                join account in Accounts on user.Id equals account.UserId
                join history in History on account.Id equals history.AccountId
                    into operationsHistory
                where user.Login.Equals(login) && user.Password.Equals(password)
                select new { Account = account, Histories = operationsHistory }).ToDictionary(res => res.Account, res => res.Histories);
            return accountsAndHistories;
        }

        /// <summary>
        /// Получить все операции пополения счёта с указанием владельца.
        /// </summary>
        /// <returns>Владелец счета, его аккаунты и операции пополнения.</returns>
        public Dictionary<User, Dictionary<Account, IEnumerable<OperationsHistory>>> GetUsersAccountsOperationsInput()
        {
            var usersAccountsOperationsInput = (from user in Users
                join account in Accounts on user.Id equals account.UserId into @a
                select new
                {
                    User = user,
                    Accounts = (from acc in @a
                        join history in History on acc.Id equals history.AccountId into @h
                        select new
                        {
                            Account = acc,
                            History = from operation in @h
                                where operation.OperationType.Equals(OperationType.InputCash)
                                select operation
                        }).ToDictionary(res => res.Account, res => res.History)
                }).ToDictionary(res => res.User, res => res.Accounts);
            return usersAccountsOperationsInput;
        }

        /// <summary>
        /// Получить пользователей у которых на счету больше n
        /// </summary>
        /// <param name="n">Сумма на счету</param>
        /// <returns>Пользователи.</returns>
        public IEnumerable<User> GetUsersAccountSumMoreN(decimal n)
        {
            var usersAccountSumMoreN = (from user in Users
                join account in Accounts on user.Id equals account.UserId
                where account.CashAll > n
                select user).Distinct();
            return usersAccountSumMoreN;
        }
    }
}