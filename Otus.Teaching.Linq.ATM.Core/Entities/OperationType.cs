﻿using System.ComponentModel;

namespace Otus.Teaching.Linq.ATM.Core.Entities
{
    public enum OperationType
    {
        [Description("Зачисление")]
        InputCash = 1,
        [Description("Снятие")]
        OutputCash = 2
    }
}