﻿using System;
using System.Globalization;

namespace Otus.Teaching.Linq.ATM.Core.Entities
{
    public class OperationsHistory
    {
        public int Id { get; set; }
        public DateTime OperationDate { get; set; }
        public OperationType OperationType { get; set; }
        public decimal CashSum { get; set; }
        public int AccountId { get; set; }

        public override string ToString()
        {
            
            return $"Дата операции: {OperationDate.ToString(CultureInfo.CurrentCulture)}. " +
                   $"Тип операции: {GetOperationTypeDesctiption()}. " +
                   $"Сумма операции: {CashSum.ToString(CultureInfo.CurrentCulture)}";
        }

        private string GetOperationTypeDesctiption()
        {
            var operationType = OperationType switch
            {
                OperationType.InputCash => "Пополнение",
                OperationType.OutputCash => "Снятие",
                _ => OperationType.ToString()
            };

            return operationType;
        }
    }
}